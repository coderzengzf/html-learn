const imgs = document.querySelectorAll('.container img');
const pointer = document.querySelector('.pointer');
for (const img of imgs) {
  // console.log('img', img);
  img.onmouseenter = () => {
    const s = img.offsetWidth;
    const h = img.offsetHeight;
    const x = img.offsetLeft;
    const y = img.offsetTop;
    pointer.style.setProperty('--x', x + 'px');
    pointer.style.setProperty('--y', y + 'px');
    pointer.style.setProperty('--s', s + 'px');
    pointer.style.setProperty('--h', h + 'px');
  }
}