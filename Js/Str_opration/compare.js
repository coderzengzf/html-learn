/**
 * 比较两个字符串的大小
 * 两个字符串都是用-连接的数字，比如 1-2-33-41-5
 * 比较的方式是从左到右，依次比较每个数字的大小，遇到相等的数字继续往后比较，遇到不同
   的数字直接得到比较结果
 * s1 > s2 return 1
 * s1 < s2 return -1
 * s1 === s2 return 0
 */

function compare(s1, s2){
    const iter1 = walk(s1)
    const iter2 = walk(s2)
    while(1){
        // 取出 s1 的下一个数字
        const n1 = iter1.next();
        // 取出 s2 的下一个数字
        const n2 = iter2.next();
        if(n1.done && n2.done){
            return 0
        }
        else if(n1.done){
            return -1
        }
        else if(n2.done){
            return 1
        }
        else if(n1.value > n2.value){
            return 1
        }
        else if(n1.value < n2.value){
            return -1
        }
    }
}

function* walk(str){
    let part = ''
    for(let i = 0; i < str.length; i++){
        if(str[i] !== '-'){
            part += str[i]
        }else {
            yield +part;
            part = '';
        }
    }
    if(part){
        yield +part;
    }
}

console.log(compare('3-5-56-7-1', '3-5-41-2-5'));