// 防抖
// 防抖就是值触发事件后在n秒内函数值执行一次，如果在n秒内又触发了事件，则会重新计算函数时间
function debounce(func, delay){
  let timer;
  return function(){
    if(timer) clearTimeout(timer);
    timer = setTimeout(() => {
      func();
    },delay)
  }
}