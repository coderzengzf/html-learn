const obj = {
    a: 0
}

obj['1'] = 0
obj[++obj.a] = obj.a++
console.log('obj', obj);
const values = Object.values(obj)
console.log('values', values);
obj[values[1]] = obj.a
console.log(obj);