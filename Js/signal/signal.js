// 设计一个程序实现交通灯切换
const serial = ['Red', 'Yellow', 'Green']

/**
 * 
 * @param {*} duration 
 * @returns 
 */
function delay(duration = 1000){
  return new Promise(resolve => {
    setTimeout(resolve, duration)
  })
}

class Signal {
  get next(){
    return serial[(serial.indexOf(this.sig) + 1) % serial.length]
  }

  get remain(){
    let diff = this.end- Date.now();
    if(diff < 0){
      diff = 0
    }
    return diff / 1000
  }

  /**
   * 
   * @param {*} option 
   */

  constructor(option){
    this.sig = option.init;
    this.times = option.times;
    this.eventMap = new Map();
    this.eventMap.set('change', new Set())
    this.eventMap.set('tick', new Set())
    this.setTime();
    this.exchange();
  }

  /**
   * 
   * @param {*} event 
   * @param {*} handler 
   */
  on(event, handler){
    this.eventMap.get(event).add(handler)
  }

  off(event, handler){
    this.eventMap.get(event).delete(handler)
  }

  emit(event){
    this.eventMap.get(event).forEach(h => {
      h.call(this, this)
    })
  }

  async exchange(){
    await 1;
    if(this.remain > 0){
      // console.log(this.remain);
      this.emit('tick')
      await delay(1000)
    }else {
      this.sig = this.next;
      this.setTime();
      this.emit('change')
      // console.log('切换了', this.sig);
    }
    this.exchange();
  }

  setTime(){
    this.start = Date.now();
    const time = this.times[serial.indexOf(this.sig)]
    this.end = this.start + time * 1000
  }
}

const s = new Signal({
  init: 'Red',
  times: [90, 3, 30]
})

s.on('tick', (e) => {
  console.log(e.sig, Math.round(e.remain));
})

// console.log(s);