// 封装filter函数
/**
 * 两个数组filter
 * @param {*} arr1 
 * @param {*} arr2 
 * @returns 
 */
function filterArr(arr1, arr2) {
  let fileId = arr1.map(item => item.fileId)
  return arr2.filter(item => !fileId.includes(item.fileId))
}

// let fileId = this.fileData.map(item => item.fileId) 
// return callback(res.data.filter(item => !fileId.includes(item.fileId)))
