/**
 * 这个文件里都是关于JS Array的一些操作
 * @type {string[]}
 */

/**
 * 两个数组遍历之后的操作
 * @type {string[]}
 */

// 所有的downloadFile fileId

let downloadFile = []
let list = [];

for (var i = 0; i < 100000; i++) {
    downloadFile.push({
        fileId: "f328035c84734212afcf83b94329ef5" + i,
        time: new Date().getTime()
    })
}

let hashMap = new Map();
let maxTime = 0;

for (var item of downloadFile) {
    let key = item['fileId'], time = item['time']
    if(hashMap.has(key)){
        if(time > maxTime){
            hashMap.set(key, time)
            maxTime = time
        }
    }else{
        hashMap.set(key, time)
        maxTime = maxTime > time ? maxTime : time
    }
}

// for (var item of downloadFile) {
//     let key = item['fileId'], time = item['time']
//     if(hashMap[key]){
//         if(time > maxTime){
//             hashMap[key] = time
//             maxTime = time
//         }
//     }else{
//         hashMap[key] = time
//         maxTime = maxTime > time ? maxTime : time
//     }
// }


for (var j = 0 ; j < 100; j++){
    if(j < 50){
        list.push({
            createTime: 1678158309,
            fileId: "f328035c84734212afcf83b94329ef5"+j,
            fileName: "E@" + j,
            fileType: 0,
            isUpload: "",
            level: 2,
            path: "edb853cc05f744f6b69316f8a8c5ef76/f328035c84734212afcf83b94329ef5b",
            size: 0,
            spaceType: 2,
            upTime: 1678158309,
            version: 1,
        })
    }else {
        list.push({
            createTime: 1678158309,
            fileId: "f328035c84734212afcf83b94329ee"+j + 2,
            fileName: "E@1" + j + 2,
            fileType: 0,
            isUpload: "",
            level: 2,
            path: "edb853cc05f744f6b69316f8a8c5ef76/f328035c84734212afcf83b94329ef5b",
            size: 0,
            spaceType: 2,
            upTime: 1678158309,
            version: 1,
        })
    }
}

/**
 * map写法
 * @param arr
 */
function unique(arr) {
    let newMap = hashMap;
    let newTime = maxTime
    for (var item of arr) {
        console.log('item', item)
        let key = item['fileId'], time = item['time']
        if(newMap.has(key)){
            if(time > newTime){
                newMap.set(key, time)
                newTime = time
            }
        }else{
            newMap.set(key, time)
            newTime = newTime > time ? newTime : time
        }
    }
    return {newMap, newTime}
}

let result = unique(list)
// console.log('hashMap', result.newMap)
// console.log('maxTime', result.newTime)

// const array1 = ['a', 'b', 'c', 'd', 'e']
//
// // 给我的List表
// const array2 = [{value: 'a'}, {value: 'b'}, {value: 'f'}, {value: 'g'}, {value: 'h'}]

// array1.map(item => {
//     return {value: item}
// })


// 双重forEach
// array2.forEach(item => {
//     array1.forEach((el,index) => {
//         if(item.value == el){
//             item['status'] = true
//             item['fileId'] = index
//         }
//     })
// })

// console.log('arr1', array1)
// console.log('arr2', array2)

// 去重
// const array1 = ['a', 'b', 'c', 'd', 'e']
// const array2 = ['a', 'b', 'c', 'd', 'e']
// let arr = array1.concat(array2)
// console.log('arr', Array.from(new Set(arr)));
