class cacheTotal {
  constructor(){
    this.cachePool = {}
  }

  /**
   * 
   * @param {*} arr 
   * @returns 
   */
  getTotral(arr){
    const arrStr = arr.toString();
    if(this.cachePool.hasOwnProperty(arrStr)){
      console.log('我使用的是缓存');
      return this.cachePool[arrStr]
    }else {
      console.log('我是新计算的缓存');
      this.cachePool[arrStr] = this.getTotalAtion(arr)
    }
  }

  /**
   * 
   * @param {*} arr 
   * @returns 
   */
  getTotalAtion(arr){
    return arr.reduce((pre, cur) => {
      return pre + cur
    },0)
  }
}

const ca = new cacheTotal();

const arr1 = [1,2,3,123]
const arr2 = [1,2,3,123]
const arr3 = [2,3,4,423]

ca.getTotral(arr1)
ca.getTotral(arr2)
ca.getTotral(arr3)