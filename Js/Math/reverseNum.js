function getReverseNumber(num) {
  let result = [];
  for (let i = 10; i <= num; i++) {
      let a = i.toString();
      let b = a.split("").reverse().join("");
      // let b = a.split("").reverse().join("");

      if (a === b) {
        result.push(b);
      }
  };
  return result;
}

let res = getReverseNumber(20);
console.log('res', res);