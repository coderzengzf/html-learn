function getPrime(num){
  let result = [];
  for(let i = 2; i <= num; i++){
    //假设所有的数都是质数
    let flag = true;
     //通过嵌套循环找到 i 除了1 和本身以外所有可能出现的因子
    for (let j = 2; j <= i / 2; j++) {
     //判断 i 是否为质数
      if (i % j == 0) { //能进到当前的分支 说明不是质数
        flag = false;
        break;
      }
    }
    if (flag) {
      result.push(i)
      // console.log(i);
    }
  }
  return result
}

let res = getPrime(1000)
console.log('res', res);